// example.test.js
const expect = require('chai').expect;
const { assert } = require('chai');
const {randomInteger,greeting} = require('../src/mylib');

describe('Unit testing mylib.js ', () => {
let today = new Date();
let date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
let time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
let dateTime = date+' '+time;

before(()=> {
    console.log("Test started at ",dateTime)
})


describe('Testing the Greeting function',()=>{
    let name = 3
    before('Checking if the name given is a string',()=> {
        assert.typeOf(name,'string')
    })
    it('Should return a greeting string based on the name given', () => {
        const result = greeting(name);
        expect(result).to.equal(`Hello ${name}`); // result expected to equal Hello {name}
        })

})

describe('Testing the RandomInteger function',()=>{

    it('Should return a random number between 0 and 99', () => {
        const rn = randomInteger();
        const result = rn >= 0 || rn <= 99 ? true:false; 
        expect(result).to.equal(true); // result expected to equal Hello {name}
        })

})



after(()=> {
    console.log("Test ended at ",dateTime)
})

});

// src/main.js
const express = require('express');
const app = express();
const port = 3000;
const {greeting,randomInteger} = require('./mylib');
app.get('/greeting', (req, res) => res.send(greeting(req.query.name)));
app.get('/random', (req, res) => {
res.send(randomInteger());
});
app.listen(port, () => {
console.log(`Server: http://localhost: ${port}`);
});

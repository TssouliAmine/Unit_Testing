// src/mylib.js
module.exports = {
    // Greets people based on their name:
    greeting:(name)=> `Hello ${name}`,
    // Returns a random integer from 0 to 99:
    randomInteger: () => Math.floor(Math.random() * 100).toString(),
    
} 